#!/bin/sh
set -e
. ./build.sh
 
mkdir -p isodir
mkdir -p isodir/boot
mkdir -p isodir/boot/grub
 
cp sysroot/boot/holoniumos.kernel isodir/boot/holoniumos.kernel
cat > isodir/boot/grub/grub.cfg << EOF
menuentry "holoniumos" {
	multiboot /boot/holoniumos.kernel
}
EOF
grub-mkrescue -o holoniumos.iso isodir